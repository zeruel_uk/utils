/******************************************************************************
 * Copyright (C) 2017-2018 David Bowman
 * You may distribute and modify this code under the terms of the
 * CreativeCommons (CC0) License
 *****************************************************************************/
#ifndef ZERUEL_UTILS_TESTUTILS_H
#define ZERUEL_UTILS_TESTUTILS_H

#include <sstream>
#include <iostream>
#include <vector>

/******************************************************************************
 * \brief A helper to provide pretty output when writing tests
 *****************************************************************************/
class Test
{
  // formatting
  std::size_t const INDENT;
  std::string const RED   ;
  std::string const GREEN ;
  std::string const RESET ;

  // status
  std::size_t m_count;
  std::size_t m_fails;

  // groups  
  std::vector<std::string> groups;

  std::string repeat( std::size_t count, const char * val )
  {
    std::stringstream ss;
    for( std::size_t i( 0 ); i < count; ++i )
    {
      ss << val;
    }
    return ss.str();
  }

public:

  Test(std::size_t indent = 2 )
    : INDENT ( indent       )
    , RED    ( "\033[0;31m" )
    , GREEN  ( "\033[0;32m" )
    , RESET  ( "\033[0m"    )
    , m_count( 0            )
    , m_fails( 0            )
  {
  }

  ~Test()
  {
    print_report();
  }

  void print_report()
  {
    std::stringstream ss;
    ss << repeat( 75, "*" ) << "\n";

    if( m_fails == 0 )
    {
      ss << GREEN << "  SUCCESS" << RESET 
         << " : All " << m_count << " tests completed successfully." << "\n";
    }
    else
    {
      ss << RED << "  Error" << RESET 
         << " : " << m_fails << " of " << m_count << " tests failed." << "\n";
    }

    ss << repeat( 75, "*" ) << "\n";
    std::cout << ss.str() << std::endl;
  }

  int return_value()
  {
    return m_fails;
  }

  void begin_group( std::string const & group )
  {
    std::stringstream ss;
    ss << repeat( groups.size() * INDENT, " ");
    ss << "Begin tests for '" << group << "'";
    std::cout << ss.str() << std::endl;
    groups.emplace_back( group );
  }

  void end_group( )
  {
    if( groups.empty() )
    {
      std::cout << "Error closing group" << std::endl;
      return;
    }

    std::string group( groups.back() );
    groups.pop_back();

    std::stringstream ss;
    ss << repeat( groups.size() * INDENT, " ");
    ss << "End tests for '" << group << "'";
    std::cout << ss.str() << std::endl << std::endl;
#ifdef TEST_UTILS_GROUP_FAILURES
    if( m_fails > 0 )
    {
      print_report();
      std::cout << "Note: tests exited early after failure in group" << std::endl;
      exit( 1 );
    }
#endif
  }

  void test( bool condition, std::string const & msg )
  {
    std::stringstream ss;

    ss << repeat( groups.size() * INDENT, " " );
    ss << "Testing " << std::string( msg ) << " ";

    std::size_t diff( 70 - ss.str().size() );
    if( diff < 70)
    {
      ss << repeat( diff, "." );
    }

    ++m_count;

    if( condition )
    {
      ss << GREEN << " PASS" << RESET;
    }
    else
    {
      ss << RED << " FAIL" << RESET;
      ++m_fails;
    }

    std::cout << ss.str() << std::endl;
  }

  template< bool BOOL >
  void static_test( std::string const & msg )
  {
      test( BOOL, msg );
  }

  template< typename A, typename B >
  void static_test( std::string const & msg )
  {
      test( std::is_same< A, B >::value, msg );
  }
};

#endif // ZERUEL_UTILS_TESTUTILS_H
