/******************************************************************************
 * Copyright (C) 2017-2018 David Bowman
 * You may distribute and modify this code under the terms of the
 * CreativeCommons (CC0) License
 *
 * Inspired by the non-variadic type list described by Andrei Alexandrescu
 * in his book 'Modern C++ Design'.
 *****************************************************************************/
#ifndef ZERUEL_UTILS_META_TYPE_LIST_H
#define ZERUEL_UTILS_META_TYPE_LIST_H

#include <type_traits>
#include <limits>

/******************************************************************************
 * \brief A type to hold a list of types
 *****************************************************************************/
template< typename... Types >
struct type_list { };

/******************************************************************************
 * \brief Apply the types in a type_list to variadic type (e.g std::tuple )
 *****************************************************************************/
template< template< typename... > class Target, typename... Types >
struct type_list_apply;


template< template< typename... > class Target, typename... Types >
struct type_list_apply< Target, type_list< Types... > >
{
  typedef Target< Types... > result_type;
};

/******************************************************************************
 * \brief Check if two type_lists are the same, forwards to std::is_same but
 *        is here for consistancy
 *****************************************************************************/
template< typename A, typename B >
struct type_list_equal
    : std::false_type
{
};

template< typename... As, typename... Bs >
struct type_list_equal< type_list<As...>, type_list<Bs...> >
    : std::is_same< type_list<As...>, type_list<Bs...> >
{
};

/******************************************************************************
 * \brief Return the number of entries in a type_list (parameter pack size)
 *****************************************************************************/
template< typename TypeList >
struct type_list_size;

template< typename... Types >
struct type_list_size< type_list< Types... > >
{
  typedef std::size_t value_type;
  static constexpr value_type value = sizeof...( Types );
};

/******************************************************************************
 * Implimentation details for remove and find are in here
 *****************************************************************************/
#include "TypeList_private.h"

/******************************************************************************
 * \brief Remove all instances of type 'Remove' from a type_list
 *****************************************************************************/
template< typename Remove, typename TypeList >
struct type_list_remove_all;

template< typename Remove, typename... Types >
struct type_list_remove_all< Remove, type_list< Types... > >
{
    // result is a type_list with all instances of 'Remove' removed
    typedef typename type_list_remove_impl< Remove
                                          , true // all
                                          , type_list< Types... >
                                          , type_list<>
                                          >::result             result;
};

/******************************************************************************
 * \brief Remove the first instances of type 'Remove' from a type_list
 *****************************************************************************/
template< typename Remove, typename TypeList >
struct type_list_remove_one;

template< typename Remove, typename... Types >
struct type_list_remove_one< Remove, type_list< Types... > >
{
    // result is a type_list with the first instance of 'Remove' removed
    typedef typename type_list_remove_impl< Remove
                                          , false // not all
                                          , type_list< Types... >
                                          , type_list< >
                                          >::result             result;
};

/******************************************************************************
 * \brief Determine if 'Find' exists in the provided type_list
 *        valid indicates if it was found
 *        index indicates it's position in the input
 *****************************************************************************/
template< typename Find, typename TypeList >
struct type_list_find;

template< typename Find, typename... Types >
struct type_list_find< Find, type_list< Types... > >
{
  typedef type_list_find_impl< Find, type_list< Types... >
                             , type_list< >
                             >                result;

  static constexpr bool        valid = result::valid;
  static constexpr std::size_t index = result::index;
};


/******************************************************************************
 * \brief Concatenate the types from the provided type_lists
 *****************************************************************************/
template< typename TypeListA, typename TypeListB >
struct type_list_concat;

template< typename... As, typename... Bs >
struct type_list_concat< type_list< As...>, type_list< Bs... > >
{
  typedef type_list< As..., Bs... > result;
};


/******************************************************************************
 * \brief Remove all duplicates from the provided type_list
 *****************************************************************************/
template< typename TypeList >
struct type_list_unique;

template< typename Head, typename... Tail >
struct type_list_unique< type_list< Head, Tail... > >
{
  typedef typename type_list_remove_all< Head
                                       , type_list< Tail... >
                                       >::result             temp;
  typedef typename type_list_unique< temp >::result          new_tail;

  typedef type_list< Head >                                  new_head;


  typedef typename type_list_concat< new_head
                                   , new_tail
                                   >::result                 result;
};

// end condition for type_list_unique
template<>
struct type_list_unique< type_list<> >
{
  typedef type_list<>                                        result;
};

// TODO's
// - replace_one
// - replace_all
// - has_duplicates

#endif
