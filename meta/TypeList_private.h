/******************************************************************************
 * Copyright (C) 2017-2018 David Bowman
 * You may distribute and modify this code under the terms of the
 * CreativeCommons (CC0) License
 *
 * Inspired by the non-variadic type list described by Andrei Alexandrescu
 * in his book 'Modern C++ Design's
 *****************************************************************************/
#ifndef ZERUEL_UTILS_META_TYPE_LIST_PIVATE_H_
#define ZERUEL_UTILS_META_TYPE_LIST_PIVATE_H_

#ifndef ZERUEL_UTILS_META_TYPE_LIST_H
#error TypeList_private.h should not be included directly
#endif

///////////////////////////////////////////////////////////////////////////////
/// Remove implimentation
///////////////////////////////////////////////////////////////////////////////

template< typename Remove, bool All, typename Todo, typename Proc >
struct type_list_remove_impl;

// Base case when we have processes every entry
template< typename Remove, bool All, typename... Types >
struct type_list_remove_impl< Remove, All, type_list<>, type_list< Types... > >
{
    typedef type_list< Types... > result;
};

// When first entry of unprocessed list should not be removed, move it to processed list
template< typename Remove, bool All, typename Head, typename... Types, typename... ProcessedTypes >
struct type_list_remove_impl< Remove
                            , All
                            , type_list< Head, Types... >
                            , type_list< ProcessedTypes... >
                            >
{
    typedef typename type_list_remove_impl< Remove
                                          , All
                                          , type_list< Types... >
                                          , type_list< ProcessedTypes..., Head >
                                          >::result                      result;
};

// When first entry should be removed, ignore it and continue
template< typename Remove, typename... Types, typename... ProcessedTypes >
struct type_list_remove_impl< Remove
                            , true
                            , type_list< Remove, Types... >
                            , type_list< ProcessedTypes... >
                            >
{
    typedef typename type_list_remove_impl< Remove
                                          , true
                                          , type_list< Types... >
                                          , type_list< ProcessedTypes... >
                                          >::result                      result;
};

// When first entry should be removed concatinate lists
template< typename Remove, typename... Types, typename... ProcessedTypes >
struct type_list_remove_impl< Remove
                            , false
                            , type_list< Remove, Types... >
                            , type_list< ProcessedTypes... >
                            >
{
    typedef type_list< ProcessedTypes..., Types... >             result;
};

////////////////////////////////////////////////////////////////////////////////
/// Find Implimentation
////////////////////////////////////////////////////////////////////////////////

template< typename Find, typename Todo, typename Proc >
struct type_list_find_impl;

// 'Find' is not present in list
template< typename Find, typename... ProcessedTypes >
struct type_list_find_impl< Find
                          , type_list<>
                          , type_list< ProcessedTypes... >
                          >
{
  static constexpr bool        valid = false;
  static constexpr std::size_t index = std::numeric_limits< std::size_t>::max();
};

// 'Find' is not first in the unprocessed list
template< typename Find, typename Head, typename... Types, typename... ProcessedTypes >
struct type_list_find_impl< Find
                          , type_list< Head, Types... >
                          , type_list< ProcessedTypes... >
                          >
{
  typedef type_list_find_impl< Find
                             , type_list< Types... >
                             , type_list< ProcessedTypes..., Head >
                             >                                         recurse;

  static constexpr bool        valid = recurse::valid;
  static constexpr std::size_t index = recurse::index;
};

// 'Find' is first in the unprocessed list
template< typename Find, typename... Types, typename... ProcessedTypes >
struct type_list_find_impl< Find
                          , type_list< Find, Types... >
                          , type_list< ProcessedTypes... >
                          >
{
  // we get the index by the size of the processed list
  typedef type_list_size< type_list< ProcessedTypes... > > count;

  static constexpr bool        valid = true;
  static constexpr std::size_t index = count::value;
};

#endif // ZERUEL_UTILS_META_TYPE_LIST_PIVATE_H_
