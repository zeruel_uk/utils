 solution "type_list"

 configurations { "Debug", "Release" }

  project "type_list_tests"
     kind "ConsoleApp"
     language "C++"
     buildoptions {"-std=c++11"}
     files { "type_list_tests.cpp" }

