#include "../TypeList.h"
#include "../../TestUtils.h"

#include <type_traits>
#include <tuple>

int main( int /* argc */, char ** /* argv */ )
{
  Test tests;

  tests.begin_group( "Equality" );
  {
    typedef type_list< int, float, double >                 input_list_1a;
    typedef type_list< int, float, double >                 input_list_1b;
    typedef type_list_equal< input_list_1a, input_list_1b > result_1;

    tests.static_test< result_1::value == true >
        ( "type_list_equal returns true for the same list" );

    typedef type_list< int, float, float >                  input_list_2a;
    typedef type_list_equal< input_list_1a, input_list_2a > result_2;

    tests.static_test< result_2::value == false >
        ( "type_list_equal returns false for the differing lists" );
  }
  tests.end_group();

  tests.begin_group( "Apply" );
  {
    typedef type_list< int, float, double >                input_list;
    typedef type_list_apply< std::tuple
                           , input_list
                           >::result_type                  result;

    tests.static_test< std::is_same< std::tuple_element< 2, result >::type
                                   , double
                                   >::value
                     >( "Tuple constructed with correct types");
  }
  tests.end_group();

  tests.begin_group( "Size" );
  {
    typedef type_list< >                                    input_empty;
    typedef type_list< int, float, long >                   input_non_empty;

    tests.static_test< type_list_size< input_empty     >::value == 0 >
        ( "Empty type_list has size 0" );
    tests.static_test< type_list_size< input_non_empty >::value == 3 >
        ( "Non-empty type_list has correct size" );

  }
  tests.end_group();

  tests.begin_group( "Concatentate" );
  {
    typedef type_list< int >                                 first;
    typedef type_list< float, double >                       second;

    typedef type_list< int, float, double >                  expected;
    typedef type_list_concat< first, second >::result        result;

    tests.static_test< type_list_equal< expected, result >::value >
        ( "Concatenated type lists" );
  }
  tests.end_group();

  tests.begin_group( "Find" );
  {
    typedef type_list< int, float, double >                 input_list_1a;
    typedef type_list_find< float, input_list_1a >          result_1;

    tests.static_test< result_1::valid      >( "float was found in type list"  );
    tests.static_test< result_1::index == 1 >( "float at index 1 in type list" );

    typedef type_list_find< unsigned, input_list_1a >       result_2;
    tests.static_test< result_2::valid == false >( "unsigned was not found in type list"  );
  }
  tests.end_group();

  tests.begin_group( "Remove" );
  {
    typedef type_list< int, float, double >                 input_list_1;
    typedef type_list< float, int, float, double, double >  input_list_2;

    typedef type_list< int, double >                        expected_1;
    typedef type_list< int, float, double, double >         expected_2a;
    typedef type_list< int, double, double >                expected_2b;

    typedef type_list_remove_one< float, input_list_1 >     result_1;
    tests.static_test< type_list_equal< result_1::result, expected_1 >::value >
        ( "Removing one item from list returned correct list" );


    typedef type_list_remove_one< float, input_list_2 >     result_2a;
    tests.static_test< type_list_equal< result_2a::result, expected_2a >::value >
        ( "Removing one float from list returned correct list" );

    typedef type_list_remove_all< float, input_list_2 >     result_2b;
    tests.static_test< type_list_equal< result_2b::result, expected_2b >::value >
        ( "Removing all floats from list returned correct list" );

  }
  tests.end_group();

  tests.begin_group( "Unique" );
  {
    typedef type_list< int, float, int, float, int, float > input;
    typedef type_list< int, float >                         expected;
    typedef type_list_unique< input >::result               result;

    tests.static_test< type_list_equal< expected, result >::value >
        ( "All duplicate entries removed" );

  }
  tests.end_group();

  return tests.return_value();
}
